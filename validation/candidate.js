const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateCandidateInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : '';
  data.skills = !isEmpty(data.skills) ? data.skills : '';
  data.rolefield = !isEmpty(data.rolefield) ? data.rolefield : '';
  data.candidateexperience = !isEmpty(data.candidateexperience) ? data.candidateexperience : '';

  if (Validator.isEmpty(data.name)) {
    errors.name = 'Job job field is required';
  }

  if (Validator.isEmpty(data.skills)) {
    errors.skills = 'Company field is required';
  }

  if (Validator.isEmpty(data.rolefield)) {
    errors.rolefield = 'Role field is required';
  }

  if (!Validator.isLength(data.candidateexperience, { min: 30 })) {
    errors.candidateexperience = 'Job Description must be minimum 300 characters';
  }

  if (Validator.isEmpty(data.candidateexperience)) {
    errors.candidateexperience = 'Descrption field is required';
  }  

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
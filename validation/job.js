const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateJobInput(data) {
  let errors = {};

  data.job = !isEmpty(data.job) ? data.job : '';
  data.skills = !isEmpty(data.skills) ? data.skills : '';
  data.rolefield = !isEmpty(data.rolefield) ? data.rolefield : '';
  data.description = !isEmpty(data.description) ? data.description : '';

  if (Validator.isEmpty(data.job)) {
    errors.job = 'Job job field is required';
  }

  if (Validator.isEmpty(data.skills)) {
    errors.skills = 'Company field is required';
  }

  if (Validator.isEmpty(data.rolefield)) {
    errors.rolefield = 'Role field is required';
  }

  if (!Validator.isLength(data.description, { min: 30 })) {
    errors.description = 'Job Description must be minimum 300 characters';
  }

  if (Validator.isEmpty(data.description)) {
    errors.description = 'Descrption field is required';
  }  

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
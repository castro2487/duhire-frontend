import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import withRouter from 'react-router-dom/withRouter';
import {offerValidation} from '../../actions/profileActions'

class JobsList extends Component {
  validateOffer(e){
    e.preventDefault();
    const offerData = {
      validated: true
    }
    this.props.offerValidation(offerData, this.props.history)
  }

  render() {
    const { job, role } = this.props;
    
    return (
      <div className="card card-body bg-light mb-3">
        <div className="row">
          <div className="col-lg-6 col-md-4 col-8">
            <h3>{job.job}</h3>
            <p>
                <span>at {job.rolefield}</span>
            </p>
           </div>
          <div className="col-md-4 d-none d-md-block">
            <h4>Skill Set</h4>
            <ul className="list-group">
              {job.skills.slice(0, 4).map((skill, index) => (
                <li key={index} className="list-group-item">
                  <i className="fa fa-check pr-1" />
                  {skill}
                </li>
              ))}
            </ul>
          </div>
          {role==='Admin' ?  ' ' : <button onClick={this.validateOffer.bind(this)}>Aprobe</button>}
        </div>
      </div>
    );
  }
}

JobsList.propTypes = {
  profile: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
});

export default connect(mapStateToProps, {offerValidation})(withRouter(JobsList)
);

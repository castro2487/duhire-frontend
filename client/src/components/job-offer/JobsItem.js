import React, { Component } from 'react';
import JobsList from './JobsList'
import PropTypes from 'prop-types';

class JobsItem extends Component {
  render() {
    const { profile } = this.props;
    let jobLists

     if (profile.job.length > 0) {
        jobLists = profile.job.map(job => (
          <JobsList key={job._id} job={job} />
        ));
      }

    return (
      <div className="card card-body bg-light mb-3">
        <div className="row">   
            {jobLists}
        </div>
      </div>
    );
  }
}

JobsItem.propTypes = {
  profile: PropTypes.object.isRequired
};

export default JobsItem;

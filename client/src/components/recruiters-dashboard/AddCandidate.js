import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import TextFieldGroup from '../common/TextFieldGroup';
import TextAreaFieldGroup from '../common/TextAreaFieldGroup';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addCandidate } from '../../actions/profileActions';

class AddCandidate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      skills: '',
      rolefield: '',
      current: false,
      candidateexperience: '',
      errors: {},
      disabled: false
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onCheck = this.onCheck.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const jobData = {
      name: this.state.name,
      skills: this.state.skills,
      rolefield: this.state.rolefield,
      candidateexperience: this.state.candidateexperience
    };

    this.props.addCandidate(jobData, this.props.history);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onCheck(e) {
    this.setState({
      disabled: !this.state.disabled,
      current: !this.state.current
    });
  }

  render() {
    const { errors } = this.state;

    return (
      <div className="add-education">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <Link to="/recruiter-dashboard" className="btn btn-light">
                Go Back
              </Link>
              <h1 className="display-4 text-center">Add Job Offer</h1>
              <small className="d-block pb-3">* = required fields</small>
              <form onSubmit={this.onSubmit}>
                <TextFieldGroup
                  placeholder="* name"
                  name="name"
                  value={this.state.name}
                  onChange={this.onChange}
                  error={errors.name}
                />
                <TextFieldGroup
                  placeholder="* Skills"
                  name="skills"
                  value={this.state.skills}
                  onChange={this.onChange}
                  error={errors.skills}
                  info="Please use comma separated values (Frontend, Accountant, Lawyer)"
                />
                <TextFieldGroup
                  placeholder="* Role Field"
                  name="rolefield"
                  value={this.state.rolefield}
                  onChange={this.onChange}
                  error={errors.rolefield}
                />
                <TextAreaFieldGroup
                  placeholder="Job Experience"
                  name="candidateexperience"
                  value={this.state.candidateexperience}
                  onChange={this.onChange}
                  error={errors.candidateexperience}
                  info="Tell us about the specific need of the position"
                />
                <input
                  type="submit"
                  value="Submit"
                  className="btn btn-info btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AddCandidate.propTypes = {
  addCandidate: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  errors: state.errors
});

export default connect(mapStateToProps, { addCandidate })(
  withRouter(AddCandidate)
);

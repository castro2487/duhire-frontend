import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { deleteExperience } from '../../actions/profileActions';

class Experience extends Component {
  onDeleteClick(id) {
    this.props.deleteExperience(id);
  }

  render() {
    const job = this.props.job.map(jobs => (
      <tr key={jobs._id}>
        <td>{jobs.job}</td>
        <td>{jobs.rolefield}</td>
        <td>{jobs.description}</td>
        <td>
          <button
            onClick={this.onDeleteClick.bind(this, jobs._id)}
            className="btn btn-danger"
          >
            Delete
          </button>
        </td>
      </tr>
    ));
    return (
      <div>
        <h4 className="mb-4">Job Offers</h4>
        <table className="table">
          <thead>
            <tr>
              <th>Job Title</th>
              <th>Role</th>
              <th>Description</th>
              <th />
            </tr>
            {job}
          </thead>
        </table>
      </div>
    );
  }
}

Experience.propTypes = {
  deleteExperience: PropTypes.func.isRequired
};

export default connect(null, { deleteExperience })(Experience);

import React from "react";
import { Link } from "react-router-dom";

const ProfileActions = (props) => {
  return (
    <div className="btn-group mb-4" role="group">
      <Link to="/edit-profile" className="btn btn-light" role={props.profile.role}>
        <i className="fas fa-user-circle text-info mr-1" /> Edit Profile
      </Link>
      <Link to="/add-job-offer" className="btn btn-light">
        <i className="fab fa-black-tie text-info mr-1" />
        Add Job Offer
      </Link>
      
    </div>
  );
};

export default ProfileActions;
